// Server Auth
var express = require('express')
var jwt = require('jwt-simple')
var _ = require('lodash')
var app = express()

app.use(require('body-parser').json())

var users = [{username: 'helladyo', password: 'tezquita'}]
var secretkey = 'supersecretkey'

function findUserByUsername(username) {
    return _.find(users, {username : username})
}

function validateUser(user, password) {
    return user.password === password
}

app.post('/session', function (req, res) {
    // var username = req.body.username

    // Validate password
    var user = findUserByUsername(req.body.username)
    if (!validateUser(user, req.body.password)) {
        return res.send(401)    // Unauthorized
    }

    var token = jwt.encode({username : user.username}, secretkey)
    res.json(token)
})

app.get('/user', function (req, res) {
    var token = req.headers['x-auth']
    var user = jwt.decode(token, secretkey)
    // TODO: pull userinfo from database
    res.json(user)
})

app.listen(3000)

// Get session token:
// 1. curl -X POST -d '{"username":"helladyo"}' -H "Content-Type: application/json" localhost:3000/session
// 2. curl -X POST -d '{"username":"helladyo", "password":"tezquita"}' -H "Content-Type: application/json" localhost:3000/session
// 
// Get User From Token
// curl -H "X-Auth: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6ImhlbGxhZHlvIn0.B98f17K3psKqEpxkCodKcB1TQFgeBFHp-FlWh_BPrCQ" localhost:3000/user
// 
