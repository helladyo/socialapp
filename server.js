/*
 * @author helladyo 
 * Good stuff to put in the server:
 * * Global middleware
 * * Global configuration
 * * Telling the server to listen
 * * Logger setup
 * * Error handling
 * * Mounting controllers
 */
var express = require('express')
var bodyParser = require('body-parser')

var app = express()
app.use(bodyParser.json())

// controllers
// middleware with namespace on controller
app.use('/api/posts', require('./controllers/api/posts'))

// root endpoint
// next to inclusions are equivalents
app.use(require('./controllers/static'))
// app.use('/', require('./controllers/static'))

app.listen(3000, function() {
    console.log('Server for social app listening on', 3000)
})
