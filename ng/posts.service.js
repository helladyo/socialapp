// services
angular.module('app')
// service for HTTP API
.service('PostsService', function ($http) {
    this.fetch = function () {
        return $http.get('/api/posts')
    }

    this.create = function (post) {
        return $http.post('/api/posts', post)
    }
})
// console.error('error!')