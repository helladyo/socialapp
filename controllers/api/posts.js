/*
 @author helladyo 
 */

//importing the model
var Post = require('../../models/post')
// A better Express' solution
var router = require('express').Router()

// module.exports = function(app) {
	// GET posts from model
	router.get('/', function (req, res, next) {
	    Post.find()
	    .sort('-date')
		.exec(function(err, posts) {
			if (err) { return next(err) }
			res.json(posts)
		})
	})

	//POST posts with model
	router.post('/', function (req, res, next) {
	    var post = new Post({
	        username: req.body.username,
			body: req.body.body
		})

	    post.save(function (err, post) {
			if (err) { return next(err) }
			res.status(201).json(post)
		})
		//logs
	    console.log('post received! username: @' + post.username + ': ' + post.body)
	})	
// }
module.exports = router