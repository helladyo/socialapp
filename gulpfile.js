// Gulp file for the entire project
var gulp = require('gulp')
var fs = require('fs')

//for reading gulp/ js files/tasks 
fs.readdirSync(__dirname + '/gulp').forEach(function (task) {
	require('./gulp/' + task)
})
// general build project task
gulp.task('dev', ['watch:css', 'watch:js', 'dev:server'])

/*
	Hello World functions...
 */
/*gulp.task('welcome', fun ction() {
    console.log('welcome to gulp!')
})
// gulp hello world task 
gulp.task('hello', ['welcome'], function() {
	console.log('hello world')
})*/
