// for gulp css generation
var gulp 	= require('gulp')
var stylus 	= require('gulp-stylus')

// css code building
gulp.task('css', function () {
    gulp.src('css/**/*.styl')
    .pipe(stylus())
    .pipe(gulp.dest('assets'))
})
// watcher...
gulp.task('watch:css', ['css'], function () {
	gulp.watch('css/**/*.styl', ['css'])
})