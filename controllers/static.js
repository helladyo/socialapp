/*
 * @author helladyo 
 * 09 August 2015
 * 
 */
var express = require('express')
var router = require('express').Router()
var path = require('path')	// to sendFile in express

// root layout  endpoint
router.get('/', function (req, res) {
	// res.sendfile('layouts/posts.html')	//with deprecated sendfile
	// res.sendFile(path.join(__dirname + '/layouts/posts.html'))	//with path
	console.log(path.join(__dirname + '/../layouts/posts.html'))	//with path
	res.sendFile(path.join(__dirname + '/../layouts/posts.html'))	//with path
})

// __direct is a special Node variable that points to the current's file directory: /controllers
router.use(express.static(__dirname + '/../assets'))

module.exports = router