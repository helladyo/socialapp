// for server nodemon script
var gulp 	= require('gulp')
var nodemon = require('gulp-nodemon')

// task to start the server with nodemon
gulp.task('dev:server', function () {
	nodemon({ 
		script: 'server.js',
		ext: 	'js', 
		// for ignoring several restarts for non-node changes:
		ignore: ['ng*', 'gulp*', 'assets*']
	})
})