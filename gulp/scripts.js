// gulp scripts
var gulp 		= require('gulp')
var concat 		= require('gulp-concat')
var sourcemaps 	= require('gulp-sourcemaps')
var uglify 		= require('gulp-uglify')
var ngAnnotate 	= require('gulp-ng-annotate')

// js code building
gulp.task('js', function() {
	gulp.src(['ng/module.js', 'ng/**/*.js'])
	.pipe(sourcemaps.init())
		.pipe(concat('app.js'))
		.pipe(ngAnnotate().on('error', function (e) {
            	console.log('Error at Annotate ' + e)
        	})
		).pipe(uglify()
			.on('error', function (e) {
            	console.log('Error at Uglify ' + e)
        	})
    	)
	.pipe(sourcemaps.write())
	.pipe(gulp.dest('assets'))
})

// watcher...
gulp.task('watch:js', ['js'], function () {
	gulp.watch('ng/**/*.js', ['js'])
})
