// controllers...
angular.module('app')
// Now dependency inject Service into the controller instead of $http:
.controller('PostsController', function ($scope, PostsService) {
    // the function runs when the "Add Post" button is clicked
    $scope.addPost = function () {
        // Only add a post if there is a body
        if ($scope.postBody) {
            // Using API for saving posts...
            PostsService.create({
                username : 'helladyo',
                body : $scope.postBody
            }).success(function (post) {
                // unshift a new post into $scope.posts
                $scope.posts.unshift(post)
                // clear out the input field
                $scope.postBody = null
            }).error(function (err) {
                console.log('Error on saving post to database...' + err)
            })
        }
    }
    // Service for getting the posts at the beginning
    PostsService.fetch()
    .success(function (posts) {
        $scope.posts = posts
        console.log('Posts returned...')
    })
    .error(function (err) {
        console.log('Error on loading posts...' + err)
    })
})
